# {{project_name}}

{{project_description}}

## Installation

```
pip install {{project_name}}
```

## Usage

Run project:

```
docker-compose up
```

Run tests:

```
docker-compose run --rm web python manage.py test
```

Profit.

## Updating

Bump the version:

```
bumpversion manjor|minor|patch
```

Push to master