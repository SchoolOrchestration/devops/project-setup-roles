import setuptools

with open("./README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="{{project_name}}",
    version="0.0.0",
    author="{{author}}",
    author_email="{{author_email}}",
    description="{{project_description}}",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/{{repo_path}}",
    packages=['{{package_name}}'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)