# Project Setup Roles

>  Playbooks for initiating projects like a boss

## Usage:

It is recommended to use via docker

**Start a python library**

```
docker run --rm -it \
    -v ${PWD}:/code/project \
    registry.gitlab.com/SchoolOrchestration/devops/project-setup-roles \
    ansible-playbook startproject.yml
```

NB: for root_directory, put: `/code/project`